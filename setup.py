from phdb_admin import __author__, __license__, __version__, __email__
from setuptools import setup, find_packages

setup(name='phdb_admin',
    version=__version__,
    author=__author__,
    license=__license__,
    description='PhoenixDB admin tool',
    author_email=__email__,
    url='https://bitbucket.org/phoenixdb/phdb_admin',
    packages=find_packages(),
    scripts=['bin/phdb_admin_server'],
    include_package_data=True,
    zip_safe=False,
    requires=['Flask', 'ansible']
)
