# Running admin console (WIP)

0) Install dependencies
 
    # pip install Flask
    # pip install 'ansible<2'
 
Note, ansible >= 2 not supported yet.


1) Run admin console

From project root run:

    > PYTHONPATH=`pwd` ./bin/phdb_admin_server 
    
2) Open URL in browser [http://127.0.0.1:5000/](http://127.0.0.1:5000/) 

# Running ansible scripts manually

All ansible scripts and configs located in **phdb_admin/ansible** directory.

0) Current issue is that ssh send env variables and it causes a problem of instaling postgres. Workarround is to comment a line in /etc/ssh/ssh_config
    
    #SendEnv LANG LC_*

1) deploy ssh keys to all nodes. 'phdb' user (see ansible.cfg) will be created on every node:
    
    > ansible-playbook -kK keys_deploy.yml

On prompt enter user password for sudo access.

2) Install all necessary packages
    
    > ansible-playbook setup.yml

3) Initialize cluster
    
    > ansible-playbook init.yml

4) To start instances

    > ansible-playbook start.yml

5) To stop instances
    
    > ansible-playbook stop.yml

6) To check status of instances on every node

    > ansible-playbook status.yml