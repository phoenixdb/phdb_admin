#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = "0.0.1"
__author__  = "Artyom Smirnov <artyom_smirnov@me.com>"
__copyright__ = "PhoenixDB Team"
__license__ = "GPLv3"
__email__ = "phoenixdb@googlegroups.com"
__status__ = "Alpha"
