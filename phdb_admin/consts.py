import os


app_name = "PhoenixDB admin"
ansible_scripts_dir = os.path.join(os.path.dirname(__file__), 'ansible')
cluster_config_dir = os.path.join(os.path.dirname(__file__), 'temp')
config_file = os.path.join(cluster_config_dir, 'config')
hosts_file = os.path.join(cluster_config_dir, 'hosts')
ansible_cfg = os.path.join(cluster_config_dir, 'ansible.cfg')
ready_file = os.path.join(cluster_config_dir, 'ready')
