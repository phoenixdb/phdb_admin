import multiprocessing
import os
import threading
from tempfile import NamedTemporaryFile

import ansible.utils as ansible_ut
import ansible.callbacks as ansible_cb
import ansible.playbook as ansible_pb
import ansible.module_utils.basic as ansible_basic

from app import hosts_file
from phdb_admin.consts import ansible_scripts_dir, hosts_file
from phdb_admin.singleton import SingletonMixin


class PlaybookAsync(threading.Thread):
    def __init__(self, playbook, login, password, ssh_key, on_success=None):
        super(PlaybookAsync, self).__init__()
        self._playbook = playbook
        self._login = login
        self._password = password
        self._ssh_key = ssh_key
        self._on_success = on_success
        PlaybookLogger.instance().add_log(playbook)

    def run(self):
        ansible_ut.VERBOSITY = 0
        playbook_cb = PhDBAdminPlaybookCallbacks(self._playbook, verbose=ansible_ut.VERBOSITY)
        stats = ansible_cb.AggregateStats()
        runner_cb = PhDBAdminPlaybookRunnerCallbacks(stats, self._playbook, verbose=ansible_ut.VERBOSITY)

        private_key = NamedTemporaryFile(delete=False)
        private_key.write(self._ssh_key)
        try:
            print("Running playbook '%s' " % self._playbook)
            pb = ansible_pb.PlayBook(
                    playbook=os.path.join(ansible_scripts_dir, self._playbook),
                    host_list=hosts_file,
                    remote_user=self._login,
                    remote_pass=self._password,
                    private_key_file=private_key.name,
                    callbacks=playbook_cb,
                    runner_callbacks=runner_cb,
                    stats=stats
            )
            results = pb.run()
            private_key.close()
            playbook_cb.on_stats(pb.stats)
            print("Playbook '%s' finished" % self._playbook)

            fail = False
            unreachable = []
            for host, res in results.items():
                if res['failures'] > 0 or res['unreachable'] > 0:
                    fail = True
                if res['unreachable'] > 0:
                    unreachable.append(host)
            PlaybookLogger.instance().set_unreachable(self._playbook, unreachable)

            if fail:
                result = PLAYBOOK_STATUS_ERROR
            else:
                result = PLAYBOOK_STATUS_OK
                if self._on_success:
                    try:
                        self._on_success()
                    except Exception, e:
                        PlaybookLogger.instance().append_stdout(self._playbook, "Unable to run on_success handler: %s" % e)
                        result = PLAYBOOK_STATUS_ERROR
        except Exception, e:
            PlaybookLogger.instance().append_stdout(self._playbook, "Error during playbook execution: %s" % e)
            result = PLAYBOOK_STATUS_ERROR
        PlaybookLogger.instance().set_status(self._playbook, result)


class PhDBAdminPlaybookCallbacks(ansible_cb.PlaybookCallbacks):
    def __init__(self, playbook, verbose=False):
        self._playbook = playbook
        super(PhDBAdminPlaybookCallbacks, self).__init__(verbose)

    def on_play_start(self, name):
        super(PhDBAdminPlaybookCallbacks, self).on_play_start(name)
        PlaybookLogger.instance().append_stdout(self._playbook, "PLAY [%s]" % name)

    def on_start(self):
        super(PhDBAdminPlaybookCallbacks, self).on_start()
        PlaybookLogger.instance().append_stdout(self._playbook, 'Starting playbook %s' % self._playbook)

    def on_task_start(self, name, is_conditional):
        super(PhDBAdminPlaybookCallbacks, self).on_task_start(name, is_conditional)
        PlaybookLogger.instance().append_stdout(self._playbook, 'Starting task %s' % name)

    def on_setup(self):
        super(PhDBAdminPlaybookCallbacks, self).on_setup()
        PlaybookLogger.instance().append_stdout(self._playbook, 'Gathering facts')


class PhDBAdminPlaybookRunnerCallbacks(ansible_cb.PlaybookRunnerCallbacks):
    def __init__(self, stats, playbook, verbose=None):
        super(PhDBAdminPlaybookRunnerCallbacks, self).__init__(stats, verbose)
        self._playbook = playbook

    def on_ok(self, host, host_result):
        if host_result['invocation']['module_name'] == 'set_fact':
            for k, v in host_result['ansible_facts'].items():
                PlaybookLogger.instance().set_fact(self._playbook, host, k, v)
        super(PhDBAdminPlaybookRunnerCallbacks, self).on_ok(host, host_result)
        PlaybookLogger.instance().append_stdout(self._playbook, "[%s] OK" % host)

    def on_skipped(self, host, item=None):
        super(PhDBAdminPlaybookRunnerCallbacks, self).on_skipped(host, item)
        PlaybookLogger.instance().append_stdout(self._playbook, "[%s] SKIPPED" % host)

    def on_failed(self, host, results, ignore_errors=False):
        super(PhDBAdminPlaybookRunnerCallbacks, self).on_failed(host, results, ignore_errors)

        results2 = results.copy()
        results2.pop('invocation', None)

        item = results2.get('item', None)
        parsed = results2.get('parsed', True)
        module_msg = ''
        if not parsed:
            module_msg = results2.pop('msg', None)
        stderr = results2.pop('stderr', None)
        stdout = results2.pop('stdout', None)
        returned_msg = results2.pop('msg', None)

        msg = '[%s] FAILED' % host
        if stdout:
            msg += '\n' + stdout
        if stderr:
            msg += '\n' + stderr
        if module_msg:
            msg += '\n' + module_msg
        if returned_msg:
            msg += '\n' + returned_msg

        PlaybookLogger.instance().append_stdout(self._playbook, msg)

    def on_unreachable(self, host, results):
        super(PhDBAdminPlaybookRunnerCallbacks, self).on_unreachable(host, results)

        if self.runner.delegate_to:
            host = '%s -> %s' % (host, self.runner.delegate_to)

        item = None
        if type(results) == dict:
            item = results.get('item', None)
            if isinstance(item, unicode):
                item = ansible_ut.unicode.to_bytes(item)
            results = ansible_basic.json_dict_unicode_to_bytes(results)
        else:
            results = ansible_ut.unicode.to_bytes(results)
        host = ansible_ut.unicode.to_bytes(host)
        if item:
            msg = "fatal: [%s] => (item=%s) => %s" % (host, item, results)
        else:
            msg = "fatal: [%s] => %s" % (host, results)

        PlaybookLogger.instance().append_stdout(self._playbook, msg)


class PlaybookLogger(SingletonMixin):
    def __init__(self):
        self._manager = multiprocessing.Manager()
        self._lock = multiprocessing.Lock()
        self._playbook_statuses = self._manager.dict()

    def add_log(self, playbook):
        self._playbook_statuses[playbook] = {'status': PLAYBOOK_STATUS_RUNNING, 'stdout': '', 'facts': {}, 'unreachable': []}

    def set_status(self, playbook, status):
        self._lock.acquire()
        pb = self._playbook_statuses[playbook]
        pb['status'] = status
        self._playbook_statuses[playbook] = pb
        self._lock.release()

    def append_stdout(self, playbook, log):
        self._lock.acquire()
        pb = self._playbook_statuses[playbook]
        pb['stdout'] += log + '\n'
        self._playbook_statuses[playbook] = pb
        self._lock.release()

    def set_fact(self, playbook, host, fact, value):
        self._lock.acquire()
        pb = self._playbook_statuses[playbook]
        if host not in pb['facts']:
            pb['facts'][host] = {}
        pb['facts'][host][fact] = value
        self._playbook_statuses[playbook] = pb
        self._lock.release()

    def get_status(self, playbook):
        if playbook in self._playbook_statuses:
            return self._playbook_statuses[playbook]
        else:
            return None

    def set_unreachable(self, playbook, list):
        self._lock.acquire()
        pb = self._playbook_statuses[playbook]
        pb['unreachable'] = list
        self._playbook_statuses[playbook] = pb
        self._lock.release()


PLAYBOOK_STATUS_OK = 0
PLAYBOOK_STATUS_RUNNING = -1
PLAYBOOK_STATUS_ERROR = 1