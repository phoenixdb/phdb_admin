function get_async_playbook_status(playbook, output_element, spinner, next_button, item_with_interval) {
    $.ajax({
        url: "/async_playbook_status",
        type: "GET",
        contentType: 'application/json;charset=UTF-8',
        dataType: "json",
        cache: false,
        data: {playbook: playbook},
        success: function(json) {
            $('#' + output_element).text(json.stdout);
            if (json.status != -1 && spinner != null)
            {
                $('#' + spinner).hide();
            }
            if (json.status == 0) {
                if (next_button != null)
                    $('#' + next_button).removeClass('hidden');
                if (item_with_interval != null)
                    clearInterval(item_with_interval.interval);
            }
            if (json.status == 1) {
                $('#' + output_element).append('<span class="label label-danger">Playbook failed</span>');
                if (item_with_interval != null)
                    clearInterval(item_with_interval.interval);
            }
        },
        error: function (request, status, error) {
            $('#' + spinner).hide();
            $('#' + output_element).text("Unable to get playbook status. Error " + error);
        }
    });
}

function get_cluster_status() {
    $.ajax({
        url: "/cluster_status",
        type: "GET",
        contentType: 'application/json;charset=UTF-8',
        dataType: "json",
        cache: false,
        success: function(json) {
            $('#cluster_status_fail').hide();
            var status = json['status'];
            var ok = true;
            var item;
            for (var key in status)
            {
                item = '#item_'+key.replace(new RegExp('\\.', 'g'), '_');
                if (status[key]['phdb_status'] == '0' && status[key]['logstash_status'] == '0')
                {
                    $(item).removeClass('list-group-item-danger');
                    $(item).addClass('list-group-item-success');
                }
                else
                {
                    $(item).removeClass('list-group-item-success');
                    $(item).addClass('list-group-item-danger');
                    ok = false;
                }
            }

            var unreachable = json['unreachable'];
            for (var i = 0; i < unreachable.length; i++)
            {
                item = '#item_'+ unreachable[i].replace(new RegExp('\\.', 'g'), '_');
                $(item).removeClass('list-group-item-success');
                $(item).addClass('list-group-item-danger');
                ok = false;
            }

            item = '#item_summary';
            if (ok)
            {
                $(item).removeClass('list-group-item-danger');
                $(item).addClass('list-group-item-success');
            }
            else
            {
                $(item).removeClass('list-group-item-success');
                $(item).addClass('list-group-item-danger');
            }
        }
    }).fail(function (request, status, error) {
        $('#cluster_status_fail').removeClass('hidden');
    });
}

function switch_page()
{
    var url = window.location.href;
    var idx = url.indexOf("#");
    var hash = idx != -1 ? url.substring(idx+1) : "";
    if (hash == "")
        hash = "summary";

    $("#pages .panel").addClass("hidden");
    $("#page_" + hash).removeClass("hidden");
}

$(document).ready(function() {
switch_page();

$(window).bind('hashchange', function(){
    switch_page();
});

$("#start_cluster").click(function(){
    $.ajax({
        url: "/cluster_start_async",
        type: "GET",
        contentType: 'application/json;charset=UTF-8',
        dataType: "json",
        cache: false
    });

    var item = this;
    item.interval = setInterval(function() {
        get_async_playbook_status("start.yml", "playbook_output", null, null, item);
    }, 1000);
});

$("#stop_cluster").click(function(){
    $.ajax({
        url: "/cluster_stop_async",
        type: "GET",
        contentType: 'application/json;charset=UTF-8',
        dataType: "json",
        cache: false
    });

    var item = this;
    item.interval = setInterval(function() {
        get_async_playbook_status("stop.yml", "playbook_output", null, null, item);
    }, 1000);
});

var queries_history = [];
var history_pos = -1;

$("#query_input").keyup(function(e){
    if (e.ctrlKey) {
        if (queries_history.length > 0) {
            //History prev
            if (e.keyCode === 38) {
                if (history_pos > 0)
                    history_pos -= 1;
                var q = queries_history[history_pos];
                console.log(history_pos);
                this.value = q;
            }
            //History next
            else if (e.keyCode === 40) {
                if (history_pos < queries_history.length - 1)
                    history_pos += 1;
                var q = queries_history[history_pos];
                console.log(history_pos);
                this.value = q;
            }
        }

        //Execute query
        if (e.keyCode === 13) {
            var query = this.value;
            if (queries_history.length == 0 || (queries_history.length > 0 && queries_history[queries_history.length - 1] != query)) {
                queries_history.push(query);
                history_pos = queries_history.length;
            }

            $.ajax({
                url: "/execute_query",
                type: "GET",
                contentType: 'application/json;charset=UTF-8',
                dataType: "json",
                data: {query: query},
                cache: false,
                success: function (json) {
                    if (json.success != true) {
                        $("#results").append('<pre class="panel-danger">' + json.error + '</pre>');
                    }
                    else {
                        $("#results").append('<pre class="panel-success">Result:\n' + json.result + '</pre>');
                    }
                    window.scrollTo(0, document.body.scrollHeight);
                },
                error: function (request, status, error) {
                    $('#results').append('<pre class="panel-danger">Unable to execute query. Error ' + error + '</pre>');
                    window.scrollTo(0, document.body.scrollHeight);
                }
            });

            this.value = "";
        }
    }
    else {
        //Reset history position
        if (queries_history.length > 0)
        {
            history_pos = queries_history.length;
        }
    }
});

});