import ConfigParser
import json
import os

from flask import Flask
from flask import jsonify, render_template, request, abort

from phdb_admin.consts import app_name, cluster_config_dir, config_file, hosts_file, ansible_cfg, ready_file
from phdb_admin.playbook import PlaybookAsync, PlaybookLogger

HAVE_PHOENIXDBPY = False
try:
    import phoenixdbpy
    HAVE_PHOENIXDBPY = True
except ImportError:
    print("phoenixdbpy module not loaded")

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'


@app.route('/')
def index():
    ready = False
    if os.path.exists(ready_file):
        ready = True
    vars = {
        'app_name': app_name,
        'page_title': app_name,
        'ready': ready
    }

    if ready:
        with open(config_file, 'r') as f:
            cfg = json.load(f)
        vars['instances'] = cfg['hosts']

    return render_template('index.jinja2', **vars)


@app.route('/setup', methods=['GET', 'POST'])
def setup():
    if not os.path.exists(cluster_config_dir):
        os.mkdir(cluster_config_dir)

    step = int(request.form.get('step', default=1))
    vars = {
        'step': int(step),
        'app_name': app_name,
        'page_title': app_name + ' - Install'
    }

    cfg = {}
    need_write_config = False

    if os.path.exists(config_file):
        with open(config_file, 'r') as f:
            cfg = json.load(f)

    if step == 1:
        if 'hosts' in cfg:
            vars['hosts_str'] = '\n'.join(cfg['hosts'])
    elif step == 2:
        need_write_config = True
        hosts = request.form.get('hosts')
        hosts_list = []
        for l in hosts.split():
            l = l.strip()
            if l:
                hosts_list.append(l)
        cfg['hosts'] = hosts_list
    elif step == 3:
        need_write_config = True
        cfg['coordinator'] = request.form.get('coordinator')
        cfg['login'] = request.form.get('login')
        cfg['password'] = request.form.get('password')
        cfg['private_key'] = request.form.get('private_key')

        config = ConfigParser.RawConfigParser(allow_no_value=True)
        config.add_section('instances')
        for h in cfg['hosts']:
            config.set('instances', h)
        config.add_section('coordinator')
        config.set('coordinator', cfg['coordinator'])

        config.add_section('all:vars')
        config.set('all:vars', 'elasticsearch_host', cfg['coordinator'])
        config.set('all:vars', 'elasticsearch_port', 9200)

        with open(hosts_file, 'w') as f:
            config.write(f)

        config = ConfigParser.RawConfigParser(allow_no_value=True)
        config.add_section('defaults')
        config.set('defaults', 'hostfile', 'hosts')
        config.set('defaults', 'remote_user', cfg['login'])
        with open(ansible_cfg, 'w') as f:
            config.write(f)

        pb = PlaybookAsync('check_connection.yml', cfg['login'], cfg['password'], cfg['private_key'])
        pb.start()
    elif step == 4:
        pb = PlaybookAsync('setup.yml', cfg['login'], cfg['password'], cfg['private_key'])
        pb.start()
    elif step == 5:
        pb = PlaybookAsync('init.yml', cfg['login'], cfg['password'], cfg['private_key'])
        pb.start()
    elif step == 6:
        def on_success():
            with open(ready_file, 'w'):
                pass

        pb = PlaybookAsync('start.yml', cfg['login'], cfg['password'], cfg['private_key'], on_success)
        pb.start()

    if need_write_config:
        with open(config_file, 'w') as f:
            json.dump(cfg, f)

    vars = dict(vars, **cfg)
    return render_template('setup.jinja2', **vars)


@app.route('/async_playbook_status', methods=['GET'])
def async_playbook_status():
    playbook = request.args.get('playbook')

    status = PlaybookLogger.instance().get_status(playbook)
    if status:
        return jsonify(success=True, **status)
    else:
        return abort(404)


@app.route('/cluster_status', methods=['GET'])
def cluster_status():
    ready = False
    if os.path.exists(ready_file):
        ready = True

    if ready:
        with open(config_file, 'r') as f:
            cfg = json.load(f)
        instances = cfg['hosts']

        pb = PlaybookAsync('status.yml', cfg['login'], cfg['password'], cfg['private_key'])
        pb.start()
        pb.join()

        status = PlaybookLogger.instance().get_status('status.yml')
        return jsonify(success=True, status=status['facts'], unreachable=status['unreachable'])
    else:
        return abort(404)


@app.route('/cluster_start_async')
def cluster_start_async():
    with open(config_file, 'r') as f:
        cfg = json.load(f)

    pb = PlaybookAsync('start.yml', cfg['login'], cfg['password'], cfg['private_key'])
    pb.start()
    return jsonify(success=True)


@app.route('/cluster_stop_async')
def cluster_s():
    with open(config_file, 'r') as f:
        cfg = json.load(f)

    pb = PlaybookAsync('stop.yml', cfg['login'], cfg['password'], cfg['private_key'])
    pb.start()
    return jsonify(success=True)


@app.route('/query')
def query():
    vars = {
        'app_name': app_name,
        'have_phoenixdbpy': HAVE_PHOENIXDBPY
    }
    if HAVE_PHOENIXDBPY:
        pass
    return render_template('query.jinja2', **vars)


@app.route('/execute_query', methods=['GET'])
def execute_query():
    q = request.args.get('query')
    q = q.strip()
    q = q.strip(';')

    with open(config_file, 'r') as f:
        cfg = json.load(f)

    result = ""
    try:
        conn = phoenixdbpy.Connection(host=cfg['coordinator'])
        conn.open()
        array = conn.execute(q)
        if conn.result.selective:
            for pos, val in array:
                result += '< '
                for dim in conn.result.schema.dimensions:
                    result += '%s=%s ' % (dim.name, pos[dim.name])
                result += '> = [ '

                for attr in conn.result.schema.attributes:
                    if attr.empty_indicator:
                        continue
                    result += '%s=%s ' % (attr.name, val[attr.name])
                result += ']\n'
        else:
            result = "Success"
        conn.close()
    except Exception, e:
        return jsonify(success=False, error=str(e))

    return jsonify(success=True, result=result)


def run_app():
    app.run(threaded=False, debug=True)
